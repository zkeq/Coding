---
title: 杂类笔记存放地址
tags: [笔记]
description: 本文存放奇奇怪怪的杂乱知识..
date: 2021-12-20 00:41:01
categories: 笔记
cover: https://ik.imagekit.io/zkeq/2021-12-19/5.png
sticky: 6
---
### 算法每日一题地址: 

| ID      | 日期       | 标签  | 状态 | 笔记地址                                                     |
| ------- | :--------- | ----- | ---- | ------------------------------------------------------------ |
| No.0001 | 2021.12.18 | TIMES | DONE | [2021.12.18 1. 两数之和 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/b7fdace2-a192-49b6-bc08-2459a3e1a6e4) |
| No.0002 | 2021.12.18 | DAYS  | DONE | [2021.12.18 419. 甲板上的战舰 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/7e06ddd3-10c0-434d-a243-76cf1ce2871b) |
| No.0003 | 2021.12.19 | DYAS  | DONE | [2021.12.19 997. 找到小镇的法官 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/a5f083dc-2162-47b1-a1c8-4907b0ad7996) |
| No.0004 | 2021.12.20 | DAYS  | TODO | [2021.12.20 475. 供暖器 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/8e510a01-98f9-475e-9aad-797ecf2a9d3e) |
| No.0005 | 2021.12.21 | DAYS  | TODO | [2021.12.21 1154. 一年中的第几天 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/a2e4a20f-1f04-4042-ae08-9b7975428c16) |


### 剑指Offer算法笔记

| ID     | 日期       | 标签  | 状态 | 笔记地址                                                     |
| ------ | :--------- | ----- | ---- | ------------------------------------------------------------ |
| No.001 | 2021.12.20 | OFFER | DONE | [2021.12.20 剑指 Offer 09. 用两个栈实现队列 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/1ac7326d-38c7-43c3-9f92-baf2eccc28e2) |
| No.002 | 2021.12.20 | OFFER | DONE | [2021.12.20 剑指 Offer 30. 包含min函数的栈 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/fb18d3eb-ccdd-45b0-91fc-e63cb6f0d05f) |
| No.003 | 2021.12.21 | OFFER | DONE | [2021.12.21 剑指 Offer 24. 反转链表 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/c61cbbbe-4f29-4e13-b5fb-bd7259364451) |
| No.004 | 2021.12.21 | OFFER | TODO | [2021.12.21 剑指 Offer 06. 从尾到头打印链表 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/c1ac1a88-0dd3-47bd-a557-6000c4e638fc) |
| No.005 | 2021.12.21 | OFFER | TODO | [2021.12.21 剑指 Offer 35. 复杂链表的复制 · 语雀 (yuque.com)](https://www.yuque.com/docs/share/de34dfe7-05d0-4285-8137-a752f5a5d1e9) |

### C语言基础讲义地址:

[cpp.pdf (zkeq.xyz)](https://pdf.zkeq.xyz/?file=https://public-zkeq.oss-cn-beijing.aliyuncs.com/cpp.pdf)

### Java基础知识库地址:

[Java基础知识库 (zkeq.xyz)](https://javadocs.zkeq.xyz/#/)

### C++基础知识库地址:(简陋)

[C++ 基础知识库 (zkeq.xyz)](https://cpp.zkeq.xyz/#/)

### 提问的智慧:

[提问的智慧(中文版) (questions-zh.vercel.app)](https://questions-zh.vercel.app/#/)

[提问的智慧(中文版) (zkeq.xyz)](https://questions.zkeq.xyz/#/)

[提问的智慧(中文版) (learnonly.xyz)](https://questions.learnonly.xyz/#/)

[提问的智慧(中文版) (maylove.pub)](https://questions.maylove.pub/#/)
